package ru.itis.scheduler.utils;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.itis.scheduler.entities.Interval;
import ru.itis.scheduler.models.Event;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

@SpringBootTest()
@DisplayName("TimeUtils is working when")
public class TimeUtilsTests {

    @Autowired
    private TimeUtils timeUtils;

    @Nested
    @DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("formCommonFreeIntervals() in working when")
    public class FormCommonFreeIntervalsTests{

        @Test
        public void test_with_empty_events_list(){
            List<Interval> actual = timeUtils.formCommonFreeIntervals(getEmptyEventsList());
            System.out.println(actual);
            assertThat(actual.get(0).getEnd(), is(nullValue()));
        }

        private List<Event> getEmptyEventsList(){
            return new ArrayList<>();
        }

        @Test
        public void test_with_one_event(){
            List<Event> arg = getOneEventList();
            List<Interval> actual = timeUtils.formCommonFreeIntervals(arg);
            assertThat(actual.size(), is(2));
            assertThat(actual.get(0).getEnd(), is(arg.get(0).getStartTime()));
            assertThat(actual.get(1).getStart(), is(arg.get(0).getEndTime()));
            assertThat(actual.get(1).getEnd(), is(nullValue()));
        }

        private List<Event> getOneEventList(){
            LocalDateTime now = getCurrentLocalDateTime();
            return List.of(
                Event.builder()
                     .startTime(now.plusMinutes(2))
                     .endTime(now.plusMinutes(3))
                     .build()
            );
        }

        private LocalDateTime getCurrentLocalDateTime(){
            return LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
        }

        @Test
        public void test_with_two_not_overlap_events(){
            List<Event> arg = getTwoNotOverlapEventList();
            List<Interval> actual = timeUtils.formCommonFreeIntervals(arg);
            assertThat(actual.size(), is(3));
            assertThat(actual.get(0).getEnd(), is(arg.get(0).getStartTime()));
            assertThat(actual.get(1).getStart(), is(arg.get(0).getEndTime()));
            assertThat(actual.get(1).getEnd(), is(arg.get(1).getStartTime()));
            assertThat(actual.get(2).getStart(), is(arg.get(1).getEndTime()));
            assertThat(actual.get(2).getEnd(), is(nullValue()));
        }

        private List<Event> getTwoNotOverlapEventList(){
            LocalDateTime now = getCurrentLocalDateTime();
            return List.of(
                    Event.builder()
                            .startTime(now.plusMinutes(2))
                            .endTime(now.plusMinutes(3))
                            .build(),
                    Event.builder()
                            .startTime(now.plusMinutes(5))
                            .endTime(now.plusMinutes(6))
                            .build()
            );
        }

        @Test
        public void test_with_two_overlap_events(){
            List<Event> arg = getTwoOverlapEventList();
            List<Interval> actual = timeUtils.formCommonFreeIntervals(arg);
            assertThat(actual.size(), is(2));
            assertThat(actual.get(0).getEnd(), is(arg.get(0).getStartTime()));
            assertThat(actual.get(1).getStart(), is(arg.get(1).getEndTime()));
            assertThat(actual.get(1).getEnd(), is(nullValue()));
        }

        private List<Event> getTwoOverlapEventList(){
            LocalDateTime now = getCurrentLocalDateTime();
            return List.of(
                    Event.builder()
                            .startTime(now.plusMinutes(2))
                            .endTime(now.plusMinutes(4))
                            .build(),
                    Event.builder()
                            .startTime(now.plusMinutes(3))
                            .endTime(now.plusMinutes(5))
                            .build()
            );
        }

        @Test
        public void test_with_two_not_overlap_events_borders_check(){
            List<Event> arg = getTwoNotOverlapEventBordersCheckList();
            List<Interval> actual = timeUtils.formCommonFreeIntervals(arg);
            System.out.println(actual);
            assertThat(actual.size(), is(2));
            assertThat(actual.get(0).getEnd(), is(arg.get(0).getStartTime()));
            assertThat(actual.get(1).getStart(), is(arg.get(1).getEndTime()));
            assertThat(actual.get(1).getEnd(), is(nullValue()));
        }

        private List<Event> getTwoNotOverlapEventBordersCheckList(){
            LocalDateTime now = getCurrentLocalDateTime();
            return List.of(
                    Event.builder()
                            .startTime(now.plusMinutes(2))
                            .endTime(now.plusMinutes(3))
                            .build(),
                    Event.builder()
                            .startTime(now.plusMinutes(3))
                            .endTime(now.plusMinutes(4))
                            .build()
            );
        }

        @Test
        public void test_with_past_event(){
            List<Interval> actual = timeUtils.formCommonFreeIntervals(getOnePastEventList());
            assertThat(actual.size(), is(1));
            assertThat(actual.get(0).getEnd(), is(nullValue()));
        }

        private List<Event> getOnePastEventList(){
            LocalDateTime now = getCurrentLocalDateTime();
            return List.of(
                    Event.builder()
                            .startTime(now.minusMinutes(3))
                            .endTime(now.minusMinutes(2))
                            .build()
            );
        }

        @Test
        public void test_with_same_starts(){
            List<Event> events = getSameStartsEventList();
            List<Interval> actual = timeUtils.formCommonFreeIntervals(events);
            List<Interval> expected = getSameStartsExceptedResult(events);
            System.out.println(expected);
            System.out.println(actual);
            assertThat(actual.size(), is(expected.size()));
            assertThat(actual.get(0).getEnd(), is(expected.get(0).getEnd()));
            assertThat(actual.get(1).getStart(), is(expected.get(1).getStart()));
            assertThat(actual.get(1).getEnd(), is(expected.get(1).getEnd()));
        }

        private List<Event> getSameStartsEventList(){
            LocalDateTime now = getCurrentLocalDateTime();
            return List.of(
                    Event.builder()
                            .startTime(now.plusMinutes(2))
                            .endTime(now.plusMinutes(22))
                            .build(),
                    Event.builder()
                            .startTime(now.plusMinutes(2))
                            .endTime(now.plusMinutes(12))
                            .build()
            );
        }

        private List<Interval> getSameStartsExceptedResult(List<Event> events){
            return List.of(
                    new Interval(events.get(0).getStartTime().minusMinutes(2), events.get(0).getStartTime()),
                    new Interval(events.get(0).getEndTime(), null)
            );
        }

    }

}
