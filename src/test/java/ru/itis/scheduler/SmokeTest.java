package ru.itis.scheduler;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.itis.scheduler.controllers.AccountController;
import ru.itis.scheduler.controllers.EventController;
import ru.itis.scheduler.repositories.AccountRepository;
import ru.itis.scheduler.repositories.EventRepository;
import ru.itis.scheduler.services.AccountService;
import ru.itis.scheduler.services.EventService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@DisplayName("Schedule Server starts up when")
class SmokeTest {

    @Autowired
    private AccountController accountController;
    @Autowired
    private EventController eventController;
    @Autowired
    private AccountService accountService;
    @Autowired
    private EventService eventService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private EventRepository eventRepository;

    @Test
    void contextLoads() {
        checkControllers();
        checkServices();
        checkRepositories();
    }

    private void checkControllers() {
        assertThat(accountController, is(notNullValue()));
        assertThat(eventController, is(notNullValue()));
    }

    private void checkServices() {
        assertThat(accountService, is(notNullValue()));
        assertThat(eventService, is(notNullValue()));
    }

    private void checkRepositories() {
        assertThat(accountRepository, is(notNullValue()));
        assertThat(eventRepository, is(notNullValue()));
    }

}
