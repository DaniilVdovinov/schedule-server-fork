package ru.itis.scheduler.entities.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EventDto {

    private Long id;
    private Long accountId;
    // TODO подозреваю, что эти поля будут "отдаваться на фронт" в виде объекта, а не в виде строки. 
    // Для конвертации в строку можно использовать @JsonSerialize и @JsonDeserialize
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String description;

}
