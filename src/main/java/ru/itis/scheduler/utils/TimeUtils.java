package ru.itis.scheduler.utils;

import org.springframework.stereotype.Component;
import ru.itis.scheduler.entities.Interval;
import ru.itis.scheduler.models.Event;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class TimeUtils {

    public List<Interval> formCommonFreeIntervals(List<Event> events) {
        // Сохраняем текущее время как текущую точку, с которой мы потенциально можем найти свободный интервал
        LocalDateTime current = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
        // Сохраняем текущее время (чтобы считать свободные интервалы только для будущего - в прошлом они уже упущены)
        LocalDateTime finalCurrent = current;
        List<Interval> intervals = events.stream()
                // Убираем события, которые уже закончились (они никак не повлияют на свободные интервалы)
                .filter(event -> event.getEndTime().isAfter(finalCurrent))
                // Преобразовываем события в интервалы времени (доп.информация не нужна)
                .map(event -> new Interval(event.getStartTime(), event.getEndTime()))
                // Сортируем события по времени их начала
                .sorted(Comparator.comparing(Interval::getStart))
                .toList();
        int currentIndex = 0;
        List<Interval> result = new ArrayList<>();
        // Проходим по всем событиям
        while (currentIndex != intervals.size()) {
            /*
            Если есть следующее событие и оно началось до нашего текущего времени - оно точно не даст нам
            свободный промежуток. Мы можем его пропустить
             */
            while (currentIndex != intervals.size() && intervals.get(currentIndex).getStart().isBefore(current)) {
                /*
                Если следующее событие закончится позже, чем наша текущая точка - сдвигаем её на время окончания события
                 */
                if (intervals.get(currentIndex).getEnd().isAfter(current)){
                    current = intervals.get(currentIndex).getEnd();
                }
                // сдвигаем счётчик
                currentIndex++;
            }
            // Если мы не достигли конца списка событий
            if (currentIndex != intervals.size()) {
                /*
                Проверяем, равно ли время окончания последнего события началу следующего (крайний случай,
                если разрыва нет - свободного интервала тоже нет)
                 */
                if (!current.equals(intervals.get(currentIndex).getStart())){
                    // Добавляем свободный интервал
                    result.add(new Interval(current, intervals.get(currentIndex).getStart()));
                }
                // Сдвигаем текущую точку на конец следующего события
                current = intervals.get(currentIndex).getEnd();
                // сдвигаем счётчик
                currentIndex++;
            }
        }
        /*
        на данном этапе в current мы будем иметь время окончания последнего события. И всё время дальше будет свободным.
        Фиксируем это как интервал с неопределённым правым значением
         */
        result.add(new Interval(current, null));
        return result;
    }

}
