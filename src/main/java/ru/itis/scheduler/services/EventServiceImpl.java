package ru.itis.scheduler.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.scheduler.converters.dto.EventConverter;
import ru.itis.scheduler.entities.Interval;
import ru.itis.scheduler.entities.dtos.EventDto;
import ru.itis.scheduler.exceptions.intervals.EmptyAccountIdsException;
import ru.itis.scheduler.exceptions.persistence.EntityNotExistsException;
import ru.itis.scheduler.exceptions.persistence.EventDateOverlapException;
import ru.itis.scheduler.models.Event;
import ru.itis.scheduler.repositories.EventRepository;
import ru.itis.scheduler.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository repository;
    private final TimeUtils timeUtils;

    @Override
    public EventDto addEvent(EventDto eventDto) {
        try {
            return EventConverter.from(repository.save(EventConverter.to(eventDto)));
        } catch (Exception exception) {
            if (exception.getMessage() != null) {
                if (exception.getMessage().contains("fk_account_id")) {
                    throw new EntityNotExistsException("account with id " + eventDto.getAccountId());
                } else if (exception.getMessage().contains("date_overlap_constraint")) {
                    throw new EventDateOverlapException("" + eventDto.getAccountId());
                }
            }
            throw exception;
        }
    }

    @Override
    public List<EventDto> addEvent(List<Long> accountIds, EventDto eventDto) {
        if (accountIds.size() > 0) {
            List<EventDto> events = formEventsList(accountIds, eventDto);
            try {
                return EventConverter.from(repository.saveAll(EventConverter.to(events)));
            } catch (Exception exception) {
                if (exception.getMessage() != null) {
                    if (exception.getMessage().contains("fk_account_id")) {
                        throw new EntityNotExistsException("account with id "
                                + exception.getMessage().split("\\)")[1].substring(2));
                    } else if (exception.getMessage().contains("date_overlap_constraint")) {
                        throw new EventDateOverlapException(
                                exception.getMessage().split(",")[2].split("=\\(")[1]);
                    }
                }
                throw exception;
            }
        }
        return new ArrayList<>();
    }

    private List<EventDto> formEventsList(List<Long> accountIds, EventDto eventDto) {
        List<EventDto> events = new ArrayList<>(accountIds.size());
        for (Long id : accountIds) {
            events.add(EventDto.builder()
                    .accountId(id)
                    .startTime(eventDto.getStartTime())
                    .endTime(eventDto.getEndTime())
                    .description(eventDto.getDescription())
                    .build());
        }
        return events;
    }

    @Override
    public List<Interval> getCommonFreeIntervals(List<Long> accountIds) {
        if (accountIds.size() == 0) throw new EmptyAccountIdsException();
        List<Event> events = repository.findAllByAccountIdIn(accountIds);
        return timeUtils.formCommonFreeIntervals(events);
    }

}
