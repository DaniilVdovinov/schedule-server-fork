package ru.itis.scheduler.services;

import ru.itis.scheduler.entities.Interval;
import ru.itis.scheduler.entities.dtos.EventDto;
import ru.itis.scheduler.models.Event;

import java.util.List;

public interface EventService {

    EventDto addEvent(EventDto eventDto);
    List<EventDto> addEvent(List<Long> accountIds, EventDto eventDto);
    List<Interval> getCommonFreeIntervals(List<Long> accountIds);

}
